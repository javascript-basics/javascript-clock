function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

const CLOCK_WRAPPER_CLASS = 'clock-wrapper'
const CLOCK_ELEMENT_CLASS = 'clock-value';
const HOUR_SYSTEM_ELEMENT_CLASS = 'hour-clock-system';
const SWITCH_BTN_CLASS = 'switch-btn';

export class ClockView {
    #clockWrapper = Object.create(HTMLElement.prototype, {});
    #clockElement = Object.create(HTMLElement.prototype, {});
    #hourSystemElement = Object.create(HTMLElement.prototype, {});
    #switchBtn = Object.create(HTMLButtonElement.prototype, {});

    get hourSystemElement() {
        return this.#hourSystemElement;
    }

    get switchBtn() {
        return this.#switchBtn;
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#clockWrapper = getElementByClassName(CLOCK_WRAPPER_CLASS);
        this.#clockElement = getElementByClassName(CLOCK_ELEMENT_CLASS);
        this.#hourSystemElement = getElementByClassName(HOUR_SYSTEM_ELEMENT_CLASS);
        this.#switchBtn = getElementByClassName(SWITCH_BTN_CLASS);
    }

    updateClockWrapperOpacity(opacity) {
        this.#clockWrapper.style.opacity = opacity;
    }

    updateClockElementContent(content) {
        this.#clockElement.innerText = content;
    }

    updateClockElementRightMargin(marginRight) {
        this.#clockElement.style.marginRight = marginRight;
    }

    updateHourSystemElementOpacity(opacity) {
        this.#hourSystemElement.style.opacity = opacity;
    }

    updateHourSystemElementContent(content) {
        this.#hourSystemElement.innerText = content;
    }

    updateSwitchBtnContent(content) {
        this.#switchBtn.innerText = content;
    }
}
