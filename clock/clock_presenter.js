import {ClockService} from "./clock_service.js";
import {ClockView} from "./clock_view.js";
import {TWELVE_HOUR_SYSTEM_VALUE, TWENTY_FOUR_HOUR_SYSTEM_VALUE} from "./clock_consts.js";

const ZERO_OPACITY = '0';
const FULL_OPACITY = '1';

const MARGIN_OFFSET = 50;
const ZERO_MARGIN = '0';

const ANIMATION_TIMEOUT = 10;

export class ClockPresenter {
    #clockService = new ClockService();
    #clockView = new ClockView();

    constructor() {
        this.#init();
    }

    #init() {
        this.#initListeners();
        this.#start();
    }

    #initListeners() {
        (this.#clockView.switchBtn || new HTMLElement()).addEventListener('click',
            this.#handleSwitchBtn.bind(this));
    }

    #handleSwitchBtn() {
        this.#clockService.changeSystem();

        const opacity = this.#clockService.twentyFourHourSystem ? ZERO_OPACITY : FULL_OPACITY;
        this.#clockView.updateHourSystemElementOpacity(opacity);

        const hourSystemWith = (this.#clockView.hourSystemElement || new HTMLElement())
            .getBoundingClientRect().width;
        const marginRight = this.#clockService.twentyFourHourSystem ? `-${hourSystemWith + MARGIN_OFFSET}px` :
            ZERO_MARGIN;
        this.#clockView.updateClockElementRightMargin(marginRight);

        const hourSystem = this.#clockService.twentyFourHourSystem ? TWELVE_HOUR_SYSTEM_VALUE :
            TWENTY_FOUR_HOUR_SYSTEM_VALUE;
        const switchBtnContent = `Switch to ${hourSystem} hour system`;
        this.#clockView.updateSwitchBtnContent(switchBtnContent)
    }

    #start() {
        setInterval(this.#updateClock.bind(this), ANIMATION_TIMEOUT);
        setTimeout(this.#updateClockWrapperOpacity.bind(this), ANIMATION_TIMEOUT);
    }

    #updateClock() {
        this.#clockService.updateDate();
        this.#clockView.updateClockElementContent(this.#clockService.formattedValue);
        this.#clockView.updateHourSystemElementContent(this.#clockService.hourSystem);
    }

    #updateClockWrapperOpacity() {
        this.#clockView.updateClockWrapperOpacity(FULL_OPACITY);
    }
}
