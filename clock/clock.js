export class Clock {
    #date = new Date();

    get hours() {
        return this.#date.getHours();
    }

    get minutes() {
        return this.#date.getMinutes();
    }

    get seconds() {
        return this.#date.getSeconds();
    }

    updateDate() {
        this.#date = new Date();
    }
}
