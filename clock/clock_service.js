import {Clock} from "./clock.js";
import {TWELVE_HOUR_SYSTEM_VALUE} from "./clock_consts.js";

const AFTER_MIDDAY = 'AM';
const POST_MIDDAY = 'PM';

const NEXT_POINT = 10;
const ZERO_STR = '0';

const SWITCHING_VALUE = 0;

export class ClockService {
    #clock = new Clock();
    #twentyFourHourSystem = false;

    get twentyFourHourSystem() {
        return this.#twentyFourHourSystem;
    }

    get hourSystem() {
        return this.#clock.hours < TWELVE_HOUR_SYSTEM_VALUE ? AFTER_MIDDAY : POST_MIDDAY;
    }

    get formattedValue() {
        const convertToTwoNumberOfDigits = value => value < NEXT_POINT ? ZERO_STR + value : value;

        const hours = convertToTwoNumberOfDigits(this.#clock.hours);
        const minutes = convertToTwoNumberOfDigits(this.#clock.minutes);
        const seconds = convertToTwoNumberOfDigits(this.#clock.seconds);

        if (this.#twentyFourHourSystem) {
            return `${hours}:${minutes}:${seconds}`;
        }

        const convertedHours = ClockService.#convertHours(hours);
        return `${convertedHours}:${minutes}:${seconds}`;
    }

    static #convertHours(hours) {
        if (hours === SWITCHING_VALUE) {
            return TWELVE_HOUR_SYSTEM_VALUE;
        }

        if (hours > TWELVE_HOUR_SYSTEM_VALUE) {
            return hours - TWELVE_HOUR_SYSTEM_VALUE;
        }

        return hours;
    }

    changeSystem() {
        this.#twentyFourHourSystem = !this.#twentyFourHourSystem;
    }

    updateDate() {
        this.#clock.updateDate();
    }
}
