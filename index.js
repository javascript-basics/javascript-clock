import {ClockPresenter} from "./clock/clock_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new ClockPresenter();
        }
    }
}

Starter.start();
